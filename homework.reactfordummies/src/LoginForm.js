import './LoginForm.css';

import React, { useState } from 'react';

export default function LoginForm(props) {

    const [errorMessages, setErrorMessages] = useState({});
    const [isSubmitted, setIsSubmitted] = useState(false);

    const users = [
        {
            login: "Peter",
            password: "Great"
        },
        {
            login: "Ivan",
            password: "Terrible"
        }
    ];

    const errors = {
        login: "Invalid Login",
        password: "Invalid Password"
    };


    const handleSubmit = e => {
        e.preventDefault();

        var { login, password } = document.forms[0];

        // Find user login info
        const userData = users.find((user) => user.login === login.value);

        // Compare user info
        if (userData) {
            if (userData.password !== password.value) {
                // Invalid password
                setErrorMessages({ name: "password", message: errors.password });
            } else {
                setIsSubmitted(true);
            }
        } else {
            // Username not found
            setErrorMessages({ name: "login", message: errors.login });
        }
    };

    const renderErrorMessage = (name) => name === errorMessages.name && (
        <div className="error">{errorMessages.message}</div>
    );

    const renderForm = (
        <div className="form">
            <form onSubmit={handleSubmit}>
                <div className="input-container">
                    <label>Login: </label>
                    <input type="text" name="login" required />
                    {renderErrorMessage("login")}
                </div>
                <div className="input-container">
                    <label>Password: </label>
                    <input type="password" name="password" required />
                    {renderErrorMessage("password")}
                </div>
                <div className="button-container">
                    <input type="submit" value="Login!"/>
                </div>
            </form>
        </div>
    );

    return (
        <div>
            <div className="login-form">
                <div className="title">Sign In</div>
                {isSubmitted ? <div>Success!</div> : renderForm}
            </div>
        </div>
    );
}



